Ansible-lint
============

Dependencies
------------

Install and configure Docker or Buildah.

Test'n dev
----------

Using Docker :

```sh
docker build --pull --iidfile /tmp/iid .
docker run --rm -t \
    -v $PWD/tests:/work/tests -w /work $(cat /tmp/iid) \
    ansible-lint -v tests/playbook.yml
```

Using Podman :

```sh
podman build --pull --iidfile /tmp/iid .
podman run --rm -t \
    --security-opt label=disable \
    -v $PWD/tests:/work/tests -w /work $(cat /tmp/iid) \
    ansible-lint -v tests/playbook.yml
```
