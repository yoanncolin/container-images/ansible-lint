Ansible-lint
============

[![pipeline status](https://gitlab.com/yoanncolin/container-images/ansible-lint/badges/main/pipeline.svg)](https://gitlab.com/yoanncolin/container-images/ansible-lint/-/commits/main)

Official GitHub : [ansible/ansible-lint](https://github.com/ansible/ansible-lint)

Tags
----

* [`24.12.2`][latest], [`24.12`][latest], [`24`][latest], [`latest`][latest]
* [`24.9.2`][latest], [`24.9`][latest]
* [`24.5.0`][latest], [`24.5`][latest]
* [`24.2.3`][latest], [`24.2`][latest]
* [`24.2.2`][latest]
* [`24.2.1`][latest]
* [`6.22.2`][latest], [`6.22`][latest], [`6`][latest]
* [`6.22.1`][latest]
* [`6.22.0`][latest]
* [`6.21.1`][latest], [`6.21`][latest]
* [`6.20.3`][latest], [`6.20`][latest]
* [`6.20.2`][latest]
* [`6.20.1`][latest]
* [`6.20.0`][latest]
* [`6.18.0`][latest], [`6.18`][latest]
* [`6.17.2`][6.17.2], [`6.17`][6.17.2]
* [`6.16.0`][6.16.0], [`6.16`][6.16.0]
* [`6.15.0`][6.15.0], [`6.15`][6.15.0]
* [`6.14.6`][6.14.6], [`6.14`][6.14.6]
* [`6.14.4`][6.14.4]
* [`6.14.3`][6.14.3]
* [`6.9.1`][6.9.1], [`6.9`][6.9.1]
* [`6.8.7`][6.8.7], [`6.8`][6.8.7]
* [`6.2.1`][6.2.1], [`6.2`][6.2.1]

[latest]: https://gitlab.com/yoanncolin/container-images/ansible-lint/-/blob/main/Dockerfile
[6.17.2]: https://gitlab.com/yoanncolin/container-images/ansible-lint/-/blob/6.17.2/Dockerfile
[6.16.0]: https://gitlab.com/yoanncolin/container-images/ansible-lint/-/blob/6.16.0/Dockerfile
[6.15.0]: https://gitlab.com/yoanncolin/container-images/ansible-lint/-/blob/6.15.0/Dockerfile
[6.14.6]: https://gitlab.com/yoanncolin/container-images/ansible-lint/-/blob/6.14.6/Dockerfile
[6.14.4]: https://gitlab.com/yoanncolin/container-images/ansible-lint/-/blob/6.14.4/Dockerfile
[6.14.3]: https://gitlab.com/yoanncolin/container-images/ansible-lint/-/blob/6.14.3/Dockerfile
[6.9.1]: https://gitlab.com/yoanncolin/container-images/ansible-lint/-/blob/6.9.1/Dockerfile
[6.8.7]: https://gitlab.com/yoanncolin/container-images/ansible-lint/-/blob/6.8.7/Dockerfile
[6.2.1]: https://gitlab.com/yoanncolin/container-images/ansible-lint/-/blob/6.2.1/Dockerfile

Usage
-----

Basic usage :

```sh
docker run -t --rm -v $PWD:/work -w /work gwerlas/ansible-lint ansible-lint
```

If you have installed additional collections or roles :

```sh
docker run -t --rm \
  -v $HOME/.ansible:/root/.ansible -v $PWD:/work:z -w /work \
  gwerlas/ansible-lint ansible-lint
```

Gitlab-CI usage :

```yaml
ansible-lint:
  image: gwerlas/ansible-lint
  script: ansible-lint
```

License
-------

[BSD 3-Clause License](LICENSE).
